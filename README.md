## **Libracion de Datos**

La informacion que ponemos a consideracion en documentos en formato libre 
asi como algunos PDF's que nos sirvieron para el analisis y creacion de un
modelo de base de datos

### 00. Manual de operaciones del SICOES y clasificador instucional
- Manual de operaciones del SICOES: (nombre del documento: 00.1.Manual_de_Operaciones_del_SICOES.PDF)

- Clasificaodor instucional de Presupuestocon codigos de estructura organizacional del sector público 
        -  (nombre del documento: 00.2.Clasificador-Institucional-Gestion-2020.csv  )
        -  (nombre del documento:  00.2.Clasificador-Institucional-Gestion-2020.json)

### 01. Formularios del SICOES

    Formularios del SISTEMA DE CONTRATACIONES ESTATALES 

- Proceso A
    - Formulario 100: Inicio de proceso de Bienes 
                    (nombre documento 01.A.1--19-0374-00-962689-1-1_F100.pdf)
    - Formulario 170: Adjudicación o Declaratoria Desierta 
                    (nombre del documento 01.A.2--19-0374-00-962689-1-1_F170.pdf)     
    - Formulario 200: Información del Contrato, Orden de Compra u Orden de Servicio
                    (nombre del documento 01.A.3--19-0374-00-962689-1-1_F200.pdf)
    - Formulario 500: Recepcion de bienes, obras y servicios
                    (nombre del documento 01.A.4--19-0374-00-962689-1-1_F500.pdf)
    - Formulario 600: Resolucion de Contratos o incumplimiento a la Orden de Compra
                     u Orden de Servicio 
                    (nombre del documento 01.A.5--19-0374-00-962689-1-1_F600.pdf)
- Proceso B
    - Formulario 400 Información de Contrataciones por Excepción, por Emergencia,
                     Directas y otras que no requieren convocatoria
                    (nombre del documento 01.B.1--19-0374-00-919711-0-E_-_F400.pdf) 
    - Formulario 500 Recepcion de bienes, obras y servicios     
                    (nombre del documento 01.B.2--19-0374-00-919711-0-E_F500.pdf)
    - Formulario 250 Información de Modificaciones al Plazo y/o Monto del Contrato
                    (nombre del documento 01.B.3--19-0374-00-919711-0-E_F250.pdf)


### 02. Listado de Variables de los diferentes formularios
    
- (nombre del documento 02-Listado_de_Variables_de_los_diferentes_formularios.json)

### 03. Listado de Entidades con su descripcion     
- (nombre del documento 03_entidades_y_descripcion.json)

### 04. Listado Entidades con sus atributos o variables 
- (nombre del documento: 04-Listado_Entidades_con_sus_atributos_o_variables.json)

### 05. Modelo entidad relacion  
- Modelo entidad relacion en imagen (nombre del documento: 05-modelo_entidad_relacion_proceso_SICOES.png)
- Modelo entidad relacion editable (nombre del documento: 05.2-Modelo_entidad_relacion_proceso_SICOES.mwb)
- Modelo entidad relacion SQL (nombre del documento: 05.2-Modelo_entidad_relacion_proceso_SICOES.sql)

### 06. Identificacion del nivel de divulgacion de la informacion (OCDS)
- (nombre del documento: 06.OCDS-1.1-Basic_Intermediate_Advanced-1.1-Fields-Display)

### 07. Bibliografia de Refencia
- Documento de Guia para Contrataciones Publica a nivel Municipal (nombre del documento: 06.Guia_Contratacion_Publica_a_nivel_Municipal.pdf)
